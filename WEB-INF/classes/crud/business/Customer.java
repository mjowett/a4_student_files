// save as "<TOMCAT_HOME>\webapps\a4\WEB-INF\classes\crud\business\Customer.java"
package crud.business;

import java.io.Serializable;

public class Customer implements Serializable {

    private String fname;
    private String lname;
    private String email;

    public Customer() {
        fname = "";
        lname = "";
        email = "";
    }

    public Customer(String parFirstName, String parLastName, String parEmail) {
        this.fname = parFirstName;
        this.lname = parLastName;
        this.email = parEmail;
    }

    public String getFirstName() {
        return fname;
    }

    public void setFirstName(String parFirstName) {
        this.fname = parFirstName;
    }

    public String getLastName() {
        return lname;
    }

    public void setLastName(String parLastName) {
        this.lname = parLastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String parEmail) {
        this.email = parEmail;
    }
}
